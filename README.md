# git-lab-Demo
##  make a connection with the ssh 
- generating key
    - ```ssh-keygen -t ed25519```
- where is the key?
    - ```~.ssh/ or /home/"user"/.ssh```
    - public key is with extension ```.pub```
    - copy the public key
    - paste in public ke section on gitlab.
- connecting 
    - ```ssh -T git@example.com``` initially ```ssh -T git@gitlab.com``` enter the passphrase
- Branch
    - ```git branch branchName``` create branch ```branchName```.
    - ```git branch --list``` List all branches in git repo.
    - ```git checkout branchName``` change the current working branch to ```branchName```.
    -```git branch -d branchName```delete the branch ```branchName```.
